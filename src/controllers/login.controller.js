const db = require("../models");
const User = db.User;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const generateAccessToken = (data) => {
  return jwt.sign(data, process.env.TOKEN_SECRET, { expiresIn: "7200s" });
};

// Find a single User with an id
exports.login = async (req, res) => {
  if (String(req.body.email) === "" || String(req.body.password) === "") {
    res.status(500).send({
      error: true,
      data: [],
      message: ["Username or Password is empty."],
    });
  }

  // or you can use find one
  User.findOne({ where: { email: req.body.email } })
    .then((data) => {
      if (data) {
        bcrypt.compare(
          req.body.password,
          data.password,
          function (err, result) {
            if (result) {
              res.send({
                error: false,
                data: data,
                token: generateAccessToken({
                  id: data.id,
                  name: data.full_name,
                  email: data.email,
                }),
                message: [process.env.SUCCESS_RETRIEVED],
              });
            } else {
              res.status(500).send({
                error: true,
                data: [],
                message: ["Invalid username and Password."],
              });
            }
          }
        );
      } else {
        res.status(500).send({
          error: true,
          data: [],
          message: ["Username does not exists"],
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        error: true,
        data: [],
        message:
          err.errors.map((e) => e.message) || process.env.GENERAL_ERROR_MSG,
      });
    });
};
