const bcrypt = require("bcrypt");

exports.hashPassword = async (value) => {
  bcrypt.hash(value, parseInt(process.env.SALT_ROUNDS), function (err, hash) {
    console.log();
    return hash;
  });
};
