const express = require("express");
const dotenv = require("dotenv");
const jwt = require("jsonwebtoken");
const db = require("./src/models");
const path = require("path");
var cors = require("cors");
// routes
const userRoute = require("./src/routes/user.routes");
const loginRoute = require("./src/routes/login.routes");

var app = express();

// get config vars
dotenv.config();

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(
	express.urlencoded({
		extended: true,
	})
);

app.use(express.static(__dirname + "/public"));

// all request will go here first (middleware)
app.use(cors());
app.use((req, res, next) => {
	// you can check session here
	// console.log("Request has been sent!");

	next();
});

// check if successfully connected
db.sequelize
	.authenticate()
	.then(() => {
		console.log("Connection has been established successfully.");
	})
	.catch((err) => {
		console.error("Unable to connect to the database:", err);
	});

if (process.env.ALLOW_SYNC === "true") {
	// development mode
	db.sequelize.sync({ alter: true }).then(() => {
		console.log("Done updating the database based on Model.");
	});
}

// simple route
app.get("/", (req, res) => {
	res.json({ message: "Welcome to API demo." });
});

const authenticateToken = (req, res, next) => {
	const authHeader = req.headers["authorization"];
	const token = authHeader && authHeader.split(" ")[1];

	if (token == null) return res.sendStatus(401);

	jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
		console.log(user, err);
		if (err) return res.sendStatus(403);
		req.user = user;

		next();
	});
};

app.use("/public", express.static(path.join(__dirname + "/public/uploads/")));

// user route
app.use(`/${process.env.API_VERSION}/login`, loginRoute);
app.use(`/${process.env.API_VERSION}/user`, authenticateToken, userRoute);

// set port, listen for requests
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});

// functions
const generateAccessToken = (username) => {
	return jwt.sign(username, process.env.TOKEN_SECRET, { expiresIn: "7200s" });
};
